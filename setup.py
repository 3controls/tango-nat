"""
Install tango-nat tools
(c) by Piotr Gory, 3Controls ltd., piotr.goryl@3-controls.com, 2017
"""
from setuptools import setup

setup(name="tango-nat",
      version='1.0',
      description='Tool to run Tango device servers on a setup with port forwarding.',
      author='Piotr Goryl',
      author_email='piotr.goryl@3-controls.com',
      py_modules = ['tango-nat.tango-nat'],
      package_dir = {'tango-nat': '.'},
      scripts = ['tango-nat.py']
      )