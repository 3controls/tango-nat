(c) by Piotr Gory, 3Controls ltd., piotr.goryl@3-controls.com, 2017

Tool for starting Tango Controls device servers with port forwarding
--------------------------------------------------------------------

:file:`tango-nat` starts a device server providing it with ORBendPoint and ORBPublishEndPoint to allow it run through
NAT or any other way of port forwarding. You may need to setup port forwarding separately or modify this script to do
it automatically. It's concept is based on an original work done for the Virtual Accelerator for PL-Grid Plus project.

The script checks its name used for execution and tries to find a device server with the same name and then run it
according to provided configuration. To start your device server it is enough to make a symbolic link to tango-nat
script with a name of your device server.

To make it work with a Starter you can do the following (commands bellow assume Tango is installed in /usr/local):

1) Install the script with python setup.py:

    - For example: :command:`sudo python setup.py install --prefix=/usr/local`

2) Create a dedicated folder to be used as a path for Starter, for example:

    - :command:`sudo mkdir /usr/local/bin/natds`

3) Using Astor configure Starter to use this folder as PATH (add /usr/local/bin/natds)

4) For each of device servers create a symbolic link in the crated folder

    - example for TangoTest: :command:`sudo ln -s /usr/local/bin/tango-nat.py /usr/local/bin/natds/TangoTest`

5) Setup environment variables (for example with tangorc ):

    - TANGO_NAT_LOCAL_HOST - name or IP of local interface to bind to, default: 'localhost'
    - TANGO_NAT_PUBLIC_HOST - name or IP of remote interface (the one which will be registered
    - TANGO_NAT_PORT_RANGE_BEGIN - start of range of port number to be assigned, default: 15000
    - TANGO_NAT_PORT_RANGE_END - end of port number range, default: 25000
    - TANGO_NAT_LOCK_FILE - name of a lock file to be used to avoid ambiguous ports assignment, default: 'tango-nat.loc'
    - TANGO_NAT_ORIGINAL_DS_PATH - Where to find original device server executables, default is '/usr/local/bin:/opt/tango/bin'

6) Now your machine is ready to serve Tango in port forwarding setup :).