"""
    (c) by Piotr Gory, 3Controls Ltd., piotr.goryl@3-controls.com, 2017

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

import contextlib
import errno
import os
import time
import socket
import optparse
import sys


@contextlib.contextmanager
def file_lock(path, wait_delay=.1):
    """Provides locking with file as a semaphore."""
    while True:
        try:
            fd = os.open(path, os.O_CREAT | os.O_EXCL | os.O_RDWR)

        except OSError, e:
            if e.errno != errno.EEXIST:
                raise

            time.sleep(wait_delay)
            continue
        else:
            break
    try:
        yield fd

    finally:
        os.close(fd)
        os.unlink(path)


def is_port_available(ip, port):
    """ Check if a port is free. One can provide other way of checking but simple connecting should work."""
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((ip, int(port)))
        s.shutdown(2)
        return False

    except:
        return True


def find_free_port(ip, range_begin=15000, range_end=25000, last_assigned=None):
    """Search for an unbound port."""
    # try to search from last assigned port number (to speed up the process)
    if last_assigned is not None and last_assigned < range_end and last_assigned >= range_begin:
        for p in range(last_assigned, range_end):
            if is_port_available(ip, p):
                return p

    # next, try to go from the beginning of the range
    for p in range(range_begin, range_end):
        if is_port_available(ip, p):
            return p

    return None


def find_ds_executable(ds_name, paths):
    """Find a device server withing paths provided."""

    if ds_name.startswith('/'):
        return ds_name

    else:
        for path in paths:
            if os.path.exists(path + '/' + ds_name):
                return path + '/' + ds_name

    return None


if __name__ == '__main__':
    # environment and options
    option_parser = optparse.OptionParser()
    
    option_parser.add_option('-l', '--local-ip', '--lh', '--local-host',
                             action="store", type="string", dest="local_host",
                             default=os.getenv('TANGO_NAT_LOCAL_HOST', 'localhost'),
                             help='Set the local host or ip that a device server will bind to.')

    option_parser.add_option('-p', '--public-ip', '--public-host',
                             action="store", type="string", dest="public_host",
                             default=os.getenv('TANGO_NAT_PUBLIC_HOST'),
                             help='Public hostname or ip to be published through Tango.')

    option_parser.add_option('-b', '--begin',
                             action='store', type='int', dest='port_range_begin',
                             default=int(os.getenv('TANGO_NAT_PORT_RANGE_BEGIN', '15000')),
                             help='Specifies the lowest tcp port that can be assigned to the device server.')

    option_parser.add_option('-e', '--end',
                             action='store', type='int', dest='port_range_end',
                             default=int(os.getenv('TANGO_NAT_PORT_RANGE_END', '25000')),
                             help='Specifies the highest tcp port that can be assigned to the device server.')

    option_parser.add_option('-f', '--lock-file',
                             action='store', type='string', dest='lock_file',
                             default=os.getenv('TANGO_NAT_LOCK_FILE', 'tango-nat.loc'),
                             help='A lock file to be used to safeguard port assignment')

    option_parser.add_option('-o', '--original-path',
                             action='store', type='string', dest='original_path',
                             default=os.getenv('TANGO_NAT_ORIGINAL_DS_PATH', '/usr/local/bin:/opt/tango/bin'),
                             help='Where to find original device server executables.')

    option_parser.add_option('-d', '--device-server',
                             action='store', type='string', dest='device_server',
                             default=sys.argv[0].split('/')[-1],
                             help='Device server name')

    (options, args) = option_parser.parse_args()
    
    # search for a device server executable
    ds = find_ds_executable(options.device_server, options.original_path.split(':'))
    if ds is None:
        print 'Error: Device server not found.'
        exit(1)
    
    # safely start a device server
    with file_lock(options.lock_file):
        # find an available port
        port = find_free_port(options.local_host, options.port_range_begin, options.port_range_end)
        if port is None:
            print 'Error: No port available.'
            exit(1)

        # prepare device server arguments
        args.insert(0, options.device_server)
        args.append('-ORBendPoint')
        args.append('giop:tcp:' + options.local_host + ':' + str(port))
        args.append('-ORBendPointPublish')
        args.append('giop:tcp:' + options.public_host + ':' + str(port))

        # here one may add setting up ssh tunnel
        # ...

        # start a device server as a separate process
        os.spawnv(os.P_NOWAIT, ds, args)
